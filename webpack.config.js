var path = require('path');

module.exports = {
    entry: {
        App: path.resolve(__dirname, 'app', 'tsx', 'App.tsx')
    },
    output: {
        path: path.resolve(__dirname, 'public', 'webpack'),
        filename: '[name].js',
        publicPath: '/webpack'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.sass$/,
                use: [
                    'style-loader',
                    'css-loader',
                    {
                        loader: 'sass-loader'
                    }
                ]
            },
            {
                test: /\.jsx*$/,
                include: [
                    path.resolve(__dirname, 'app', 'jsx'),
                    path.resolve(__dirname, 'app', 'test')
                ],
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                }
            },
            {
                test: /\.tsx?$/,
                include: [
                    path.resolve(__dirname, 'app', 'tsx'),
                    path.resolve(__dirname, 'app', 'test')
                ],
                use: 'ts-loader',
                resolve: {
                    extensions: ['.jsx', '.tsx']
                }
            }
        ]
    }
};