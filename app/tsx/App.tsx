// eslint-disable-next-line no-use-before-define
import React from 'react';
import ReactDOM from 'react-dom';
import Outline from './RogueLikeGame';

// Renders the application
ReactDOM.render(<Outline />, document.getElementById('react-root'));
