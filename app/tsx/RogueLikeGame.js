"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
exports.__esModule = true;
exports.Outline = void 0;
var react_1 = require("react");
require('../css/RogueLikeGame.sass');
;
var boardDefault = new Array(50).fill(".");
var Outline = /** @class */ (function (_super) {
    __extends(Outline, _super);
    function Outline(props) {
        var _this = _super.call(this, props) || this;
        var board = boardDefault;
        _this.state = {
            board: board,
            player: {
                level: 1,
                health: 15,
                weapon: [1, 2],
                coords: {
                    x: 3,
                    y: 0
                },
                symbol: '@'
            },
            enemy: {
                level: 1,
                health: 7,
                weapon: [1, 2],
                coords: {
                    x: 10,
                    y: 0
                },
                symbol: 'T'
            }
        };
        return _this;
    }
    Outline.prototype.componentDidMount = function () {
        var _this = this;
        // Get player position and change the board
        var _a = this.state, board = _a.board, player = _a.player, enemy = _a.enemy;
        var newBoard = __spreadArrays(board);
        newBoard[player.coords.x] = player.symbol;
        newBoard[enemy.coords.x] = enemy.symbol;
        this.setState({
            board: newBoard
        });
        window.addEventListener('keydown', function (e) { return _this.handleKeyDown(e); });
    };
    Outline.prototype.componentWillUnmount = function () {
        window.removeEventListener('keydown', this.handleKeyDown);
    };
    Outline.prototype.handleKeyDown = function (e) {
        if (!e || !e.key) {
            return;
        }
        if (e.key === 'ArrowRight') {
            this.move('right');
        }
        else if (e.key === 'ArrowLeft') {
            this.move('left');
        }
    };
    /**
     * Moves the player
     * @param {string} direction
     */
    Outline.prototype.move = function (direction) {
        var _a = this.state, board = _a.board, player = _a.player;
        var newPosition;
        if (direction === 'right') {
            if (player.coords.x === board.length - 1) {
                return;
            }
            var newBoard_1 = __spreadArrays(board);
            newBoard_1[player.coords.x] = '.';
            newBoard_1[player.coords.x + 1] = player.symbol;
            this.setState(function (state) { return ({
                board: newBoard_1,
                player: __assign(__assign({}, state.player), { coords: __assign(__assign({}, state.player.coords), { x: state.player.coords.x + 1 }) })
            }); });
        }
        else if (direction === 'left') {
            if (player.coords.x === 0) {
                return;
            }
            var newBoard_2 = __spreadArrays(board);
            newBoard_2[player.coords.x] = '.';
            newBoard_2[player.coords.x - 1] = player.symbol;
            this.setState(function (state) { return ({
                board: newBoard_2,
                player: __assign(__assign({}, state.player), { coords: __assign(__assign({}, state.player.coords), { x: state.player.coords.x - 1 }) })
            }); });
        }
    };
    Outline.prototype.render = function () {
        var _a = this.state, player = _a.player, board = _a.board;
        var weaponStr = player.weapon[0] + "-" + player.weapon[1];
        var cells = board.map(function (cell, i) { return <td key={i}>{cell}</td>; });
        return (<div className="app-exterior">
                <h1 className="pageTitle">Rogue-like Game</h1>
                
                <table className="gamingBoard">
                    <tbody>
                        <tr>{cells}</tr>
                    </tbody>
                </table>
                <div className="stats">
                    <div>Level: {player.level}</div>
                    <div>Health: {player.health}</div>
                    <div>Weapon: {weaponStr} dmg</div>
                </div>
            </div>);
    };
    return Outline;
}(react_1["default"].Component));
exports.Outline = Outline;
