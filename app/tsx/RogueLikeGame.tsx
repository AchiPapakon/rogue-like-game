// eslint-disable-next-line no-use-before-define
import React, { CSSProperties } from 'react';

require('../css/RogueLikeGame.sass');

interface Coords {
    x: number;
    y: number;
}

interface Creature {
    level: number;
    health: number;
    weapon: number[];
    coords: Coords;
    symbol: string;
}

enum Direction {
    up,
    down,
    left,
    right
}

interface Cell {
    id: number;
    value: string;
    style?: CSSProperties;
}

interface OutlineState {
    board: Cell[];
    player: Creature;
    enemy: Creature;
}

const boardDefault: Cell[] = [];
for (let i = 0; i < 50; i++) {
    boardDefault.push({
        id: i,
        value: '.'
    });
}

/**
 * Takes a random value between the minimum and the maximum weapon damage,
 *  including the extreme values.
 * @param {number[]} weaponDmg Minimum and maximum weapon damage
 */
const rollWeaponDmg = (weaponDmg: number[]): number => {
    if (weaponDmg.length !== 2) {
        throw new Error('[rollWeaponDmg]: Invalid weapon damage values.');
    }

    return Math.round(Math.random() * (weaponDmg[1] - weaponDmg[0])) + weaponDmg[0];
};

export default class Outline extends React.Component<Record<string, never>, OutlineState> {
    battleTimer: NodeJS.Timeout | null;

    constructor(props: Record<string, never>) {
        super(props);

        const board = boardDefault;
        this.state = {
            board,
            player: {
                level: 1,
                health: 15,
                weapon: [1, 2],
                coords: {
                    x: 3,
                    y: 0
                },
                symbol: '@'
            },
            enemy: {
                level: 1,
                health: 5,
                weapon: [1, 2],
                coords: {
                    x: 10,
                    y: 0
                },
                symbol: 'T'
            }
        };

        this.battleTimer = null;
        this.handleKeyDown = this.handleKeyDown.bind(this);
    }

    componentDidMount(): void {
        // Get player position and change the board
        const { board, player, enemy } = this.state;
        const newBoard = [...board];
        newBoard[player.coords.x].value = player.symbol;
        newBoard[enemy.coords.x].value = enemy.symbol;

        this.setState({
            board: newBoard
        });

        this.enableMovement();
    }

    componentWillUnmount(): void {
        this.disableMovement();
    }

    handleKeyDown(e: KeyboardEvent): void {
        if (!e || !e.key) {
            return;
        }
        if (e.key === 'ArrowRight') {
            this.move(Direction.right);
        } else if (e.key === 'ArrowLeft') {
            this.move(Direction.left);
        }
    }

    enableMovement(): void {
        console.log('Movement enabled.');
        window.addEventListener('keydown', this.handleKeyDown);
    }

    disableMovement(): void {
        console.log('Movement disabled.');
        window.removeEventListener('keydown', this.handleKeyDown);
    }

    /**
     * Executes a battle round between the player and the mob.
     * @param board The game board.
     * @param currentPosition The player's position
     * @param newPosition The mob's position
     */
    battleRound(board: Cell[], currentPosition: number, newPosition: number): void {
        const newBoard = [...board];
        this.setState((state) => ({
            ...state,
            player: {
                ...state.player,
                health: state.player.health - rollWeaponDmg(state.enemy.weapon)
            },
            enemy: {
                ...state.enemy,
                health: state.enemy.health - rollWeaponDmg(state.player.weapon)
            },
            board
        }), () => {
            const { player: intervalPlayer, enemy: intervalEnemy } = this.state;
            console.log(intervalPlayer.health, intervalEnemy.health);
            if (intervalEnemy.health <= 0 && this.battleTimer !== null) {
                clearInterval(this.battleTimer);
                this.enableMovement();
                newBoard[newPosition].value = '.';
                newBoard[newPosition].style = {};
                newBoard[currentPosition].style = {};
                this.setState({
                    board: newBoard
                });
            }
        });
    }

    /**
     * Executes the whole battle between the player and the mob.
     * @param board The game board.
     * @param currentPosition The player's position
     * @param newPosition The mob's position
     */
    battle(board: Cell[], currentPosition: number, newPosition: number): void {
        this.battleRound(board, currentPosition, newPosition);
        this.battleTimer = setInterval(() => {
            this.battleRound(board, currentPosition, newPosition);
        }, 500);
    }

    /**
     * Moves the player
     * @param {Direction} direction
     */
    move(direction: Direction): void {
        const { board, player } = this.state;
        const newBoard = [...board];
        let newPosition: number;

        switch (direction) {
            case Direction.left:
                newPosition = player.coords.x - 1;
                if (player.coords.x === 0) {
                    return;
                }
                break;
            case Direction.right:
                newPosition = player.coords.x + 1;
                if (!newBoard[newPosition]) {
                    return;
                }
                break;
            default:
                throw new Error('[function move]: No valid direction given.');
        }

        // If there is a mob in 'newPosition', then do stuff
        if (newBoard[newPosition].value !== '.') {
            this.disableMovement();
            newBoard[newPosition].style = { color: 'red' };
            newBoard[player.coords.x].style = { color: 'red' };
            this.battle(newBoard, player.coords.x, newPosition);
            return;
        }

        newBoard[player.coords.x].value = '.';
        newBoard[newPosition].value = player.symbol;

        this.setState((state) => ({
            board: newBoard,
            player: {
                ...state.player,
                coords: {
                    ...state.player.coords,
                    x: newPosition
                }
            }
        }));
    }

    render(): React.ReactNode {
        const { player, board } = this.state;
        const weaponStr = `${player.weapon[0]}-${player.weapon[1]}`;
        const cells = board.map((cell) => <td key={cell.id} style={cell.style}>{cell.value}</td>);
        return (
            <div className="app-exterior">
                <h1 className="pageTitle">Rogue-like Game</h1>
                {/* <MainComponent(s) /> */}
                <table className="gamingBoard">
                    <tbody>
                        <tr>{cells}</tr>
                    </tbody>
                </table>
                <div className="stats">
                    <div>Level: {player.level}</div>
                    <div>Health: {player.health}</div>
                    <div>Weapon: {weaponStr} dmg</div>
                </div>
            </div>
        );
    }
}
